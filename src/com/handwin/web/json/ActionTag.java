package com.handwin.web.json;

import java.net.URL;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.handwin.db.HibernateTool;

public class ActionTag extends TagSupport {

	private String path;
	private String resultName;

	@Override
	public int doStartTag() throws JspException {
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest) this.pageContext
				.getRequest();
		HttpServletResponse response = (HttpServletResponse) this.pageContext
				.getResponse();
		JsonAction.clear();
		JsonAction action = null;
		try {
			if ('/' != path.charAt(0))
				action = ActionServlet.getAction("/" + path);
			else
				action = ActionServlet.getAction(path);
			ActionTools.setClient(request, response);
			Enumeration names = request.getHeaderNames();
			while (names.hasMoreElements()) {
				String name = (String) names.nextElement();
				// System.out.println(name+"="+request.getHeader(name));
				action.setHeader(name, request.getHeader(name));
			}
			action.setSession(request.getSession());
			action.setReferer(request.getHeader("referer"));
			action.setRemoteAddr(getIpAddr(request));
			action.setRealPath(this.pageContext.getServletContext()
					.getRealPath(""));
			// action.parseAttribute(request);
			if (action != null) {
				action.parseInput(request);
			}
			action.setCookies(request.getCookies());
			action.setUrl(new URL(request.getRequestURL().toString()));
			Object rlt = action.execute();
			request.setAttribute(resultName, rlt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (action != null && action.needTransaction())
				HibernateTool.closeSession();
		}
		return super.doStartTag();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	private String getIpAddr(HttpServletRequest request) {
		String ipAddress = null;
		// ipAddress = this.getRequest().getRemoteAddr();
		ipAddress = request.getHeader("x-forwarded-for");
		if (ipAddress == null || ipAddress.length() == 0
				|| "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0
				|| "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0
				|| "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}

		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
															// = 15
			if (ipAddress.indexOf(",") > 0) {
				ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
			}
		}
		return ipAddress;
	}
}
