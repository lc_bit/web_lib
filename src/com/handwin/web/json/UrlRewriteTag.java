package com.handwin.web.json;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class UrlRewriteTag extends TagSupport {

	private boolean encode=false;
	private boolean xml=false;
	

	public boolean isXml() {
		return xml;
	}

	public void setXml(boolean xml) {
		this.xml = xml;
	}

	public boolean isEncode() {
		return encode;
	}

	public void setEncode(boolean encode) {
		this.encode = encode;
	}

	
	@Override
	public int doEndTag() throws JspException {
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest) this.pageContext
				.getRequest();
		HttpServletResponse response = (HttpServletResponse) this.pageContext
				.getResponse();
		if (request.getCookies() == null || request.getCookies().length == 0) {

			// Map<String,String> mapCookie=getRequestMap(request);
			StringBuffer cookieValue = new StringBuffer();
			List<Cookie> cookies = (List<Cookie>) request
					.getAttribute("cookies");
			
			if (cookies != null && cookies.size() > 0) {
				
				for (Cookie cookie : cookies) {
						//if(xml)
						//	cookieValue.append("&amp;");
						//else
							cookieValue.append("&");
					cookieValue.append(cookie.getName());
					cookieValue.append("=");
					cookieValue.append(cookie.getValue());
					//try {
					//	cookieValue.append(java.net.URLEncoder.encode(cookie
					//			.getValue(), "utf-8"));
					//} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
					//	e.printStackTrace();
					//}
				}
			}
			if (cookieValue.length() > 0) {
				
				write(";jsessionid=");
				try {
					
					if(encode)
					{
						write(URLEncoder.encode(URLEncoder.encode(cookieValue.toString(), "utf-8"),"utf-8"));
					}
					else
					{
						write(URLEncoder.encode(cookieValue.toString(), "utf-8"));
					}
				}catch(Exception e)
				{
					
				}
				
			}

		}
		return super.doEndTag();
	}

	// private Map<String,String> getRequestMap(HttpServletRequest request)
	// {
	// Map<String,String> rlt=new HashMap<String,String>();
	// if(request.getParameter("cookies")!=null)
	// {
	// try {
	// parseCookies(request.getParameter("cookies"),rlt);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// return rlt;
	// }
	// //name=value&name=value
	// private void parseCookies(String value,Map<String,String> cookies)
	// throws Exception
	// {
	// String[] values=value.split("&");
	// if(values!=null)
	// {
	// for(int i=0;i<values.length;i++)
	// {
	// String[] nv=values[i].split("=");
	// if(nv.length!=2) continue;
	// cookies.put(nv[0], java.net.URLDecoder.decode(nv[1], "utf-8"));
	// }
	// }
	// }
	private void write(String src) {
		try {
			
			pageContext.getOut().write(src);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
