package com.handwin.util;

public class UploadFile {
	private String filedName;
	private String fileName;
	private byte[] data;
	
	public String getFiledName() {
		return filedName;
	}
	public void setFiledName(String filedName) {
		this.filedName = filedName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getData() {
		return data;
	}
	/**
	 * @param filedName 上传时参数名
	 * @param fileName 文件名
	 * @param data 文件内容
	 */
	public UploadFile(String filedName,String fileName, byte[] data) {
		super();
		this.filedName=filedName;
		this.fileName = fileName;
		this.data = data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
}
