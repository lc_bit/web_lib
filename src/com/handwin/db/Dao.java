package com.handwin.db;

import java.io.Serializable;
import java.sql.Connection;

import org.hibernate.Session;

import com.handwin.db.HException;

public interface Dao {

	public void commit();
	public void close();
	public void rollBack();
	public void insert(Object dpo) throws HException;
	public void delete(Object dpo) throws HException;
	public void save(Object dpo) throws HException;
	public Object locate(Class<?> clz, Serializable id);
	public Object locate(Class<?> clz,Object value[],String column[]);
	public Object locateLock(Class<?> clz,Object value[],String column[]);
	public Object locateLock(Class<?> clz, Serializable id);
	public Connection getConnection();
	public Session getSession();
	public void flush();
	public void clear();
}
