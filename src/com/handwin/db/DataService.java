/*
 * �������� 2005-8-18
 *
 * TODO Ҫ��Ĵ���ɵ��ļ���ģ�壬��ת��
 * ���� �� ��ѡ�� �� Java �� ������ʽ �� ����ģ��
 */
package com.handwin.db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;



/**
 * @author www
 *
 * TODO Ҫ��Ĵ���ɵ�����ע�͵�ģ�壬��ת�� ���� �� ��ѡ�� �� Java �� ������ʽ �� ����ģ��
 */
public class DataService {
	private static Logger logger = Logger.getLogger(DataService.class);
	public DataService() {
		HibernateTool.beginTransaction();
	}

	public void delete(Object po) throws HException{
		try {
			HibernateTool.getSession().delete(po);
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}

	public void insert(Object dpo) throws HException{
		try {
			HibernateTool.getSession().save(dpo);
		} catch (HibernateException ex) {
			ex.printStackTrace();
			throw new HException(ex);
		}
	}
	public void save(Object dpo) throws HException{

		try {
			HibernateTool.getSession().saveOrUpdate(dpo);
		} catch (HibernateException ex) {
			throw new HException(ex);
		}

	}

	public void begin() {
		HibernateTool.beginTransaction();
	}

	public void commit() {
		// logger.info("DataService.commit begin");
		HibernateTool.commitTransaction();
		clear();
		// logger.info("DataService.commit end");
	}

	public void rollBack() {
		// logger.info("DataService.rollBack begin");
		HibernateTool.rollbackTransaction();
		// logger.info("DataService.rollBack end");
	}

	public void close() {
		// logger.info("DataService.close begin");
		
		HibernateTool.closeSession();
		// logger.info("DataService.close end");
	}

	public void close(ResultSet result, PreparedStatement pstmt) {
		try {
			if (result != null)
				result.close();
		} catch (SQLException e1) {
			// TODO �Զ���� catch ��
			e1.printStackTrace();
		}
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (SQLException e2) {
			// TODO �Զ���� catch ��
			e2.printStackTrace();
		}
	}
	/**
	 * 根据多个字段条件，获取唯一数据，一般可以用在逻辑主键是联合主键的场景。
	 * @param clz  Hibernate类
	 * @param value 查询条件数据数组
	 * @param column 查询条件列数组
	 * @return 唯一对象，如果没有找到，返回Null
	 */
	public Object locate(Class<?> clz,Object value[],String column[])
	{
		Session session = HibernateTool.getSession();
		try
		{
			Criteria query=session.createCriteria(clz);
			
			for(int i=0;i<value.length;i++)
				query.add(Restrictions.eq(column[i], value[i]));
			query.setMaxResults(1);
			List list=query.list();
			if(list.size()>0)
				return list.get(0);
		}catch(HibernateException e)
		{
			//e.printStackTrace();
			return null;
		}
		return null;
	}
	public Object locateLock(Class<?> clz,Object value[],String column[])
	{
		Session session = HibernateTool.getSession();
		try
		{
			Criteria query=session.createCriteria(clz);
			
			for(int i=0;i<value.length;i++)
				query.add(Restrictions.eq(column[i], value[i]));
			query.setMaxResults(1);
			List list=query.list();
			if(list.size()>0){
				Object rlt= list.get(0);
				session.lock(rlt, LockMode.UPGRADE);
				return rlt;
			}
		}catch(HibernateException e)
		{
			return null;
		}
		return null;
	}
	public void flush()
	{
		Session session = HibernateTool.getSession();
		try
		{
			session.flush();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void clear()
	{
		Session session = HibernateTool.getSession();
		try
		{
			session.clear();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 根据物理ID，获取Hibernate对象
	 * @param clz Hibernate类
	 * @param id 主键
	 * @return 对象，没有找到返回Null
	 */
	public Object locate(Class<?> clz, Serializable id) {
		if(id==null)
			return null;
		Session session = HibernateTool.getSession();
		Object bpo = null;

		try {
			bpo = session.get(clz, id);
			//bpo.toString();
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
			// throw new HException(e);
		}
		return bpo;
	}
	public Object locateLock(Class<?> clz, Serializable id) {
		if(id==null)
			return null;
		Session session = HibernateTool.getSession();
		Object bpo = null;

		try {
			bpo = session.get(clz, id, LockMode.UPGRADE);
			//bpo.toString();
		} catch (HibernateException e) {
			//e.printStackTrace();
			return null;
			// throw new HException(e);
		}
		return bpo;
	}

	public Connection getConnection() {
		try {
			return HibernateTool.getSession().connection();
		} catch (HibernateException e) {
			// TODO �Զ���� catch ��
			logger.warn(e);
			e.printStackTrace();
			throw new HException(e.getMessage());
		}
	}

	public Session getSession() {
		return HibernateTool.getSession();
	}

	public static void main(String[] args) {

	}
}