/*
 * �������� 2005-8-18
 *
 * TODO Ҫ��Ĵ���ɵ��ļ���ģ�壬��ת��
 * ���� �� ��ѡ�� �� Java �� ������ʽ �� ����ģ��
 */
package com.handwin.db;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.function.ClassicAvgFunction;
import org.hibernate.dialect.function.ClassicCountFunction;
import org.hibernate.dialect.function.ClassicSumFunction;



/**
 * Basic Hibernate helper class, handles SessionFactory, Session and
 * Transaction.
 * <p>
 * Uses a static initializer for the initial SessionFactory creation and holds
 * Session and Transactions in thread local variables. All exceptions are
 * wrapped in an unchecked InfrastructureException.
 * 
 * @author christian@hibernate.org
 */
public class HibernateTool {

	private static Logger log = Logger.getLogger(HibernateTool.class);

	private static Configuration config;

	private static SessionFactory sessionFactory;

	private static final ThreadLocal threadSession = new ThreadLocal();

	private static final ThreadLocal threadTransaction = new ThreadLocal();

	private static final ThreadLocal threadInterceptor = new ThreadLocal();
	private static Map<String,ConnectionInfo> connections=new HashMap<String,ConnectionInfo>();
	
	public static Map<String, ConnectionInfo> getConnections() {
		return connections;
	}

	// Create the initial SessionFactory from the default configuration files
	static {
		try {
			config = new Configuration();

			// Properties prop = new Properties();
			// prop.setProperty("hibernate.connection.username",Configure.dbUser);
			// prop.setProperty("hibernate.connection.password",Configure.dbPassword);
			// prop.setProperty("hibernate.connection.url",Configure.dbURL);
			// config.addProperties(prop);
			Configuration classicCfg = config.configure();
			classicCfg.addSqlFunction( "count", new ClassicCountFunction());
			classicCfg.addSqlFunction( "avg", new ClassicAvgFunction());
			classicCfg.addSqlFunction( "sum", new ClassicSumFunction());

			sessionFactory = classicCfg.buildSessionFactory();
			// We could also let Hibernate bind it to JNDI:
			// configuration.configure().buildSessionFactory()
		} catch (Throwable ex) {
			// We have to catch Throwable, otherwise we will miss
			// NoClassDefFoundError and other subclasses of Error
			log.error("Building SessionFactory failed.", ex);
			ex.printStackTrace();
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Returns the SessionFactory used for this static class.
	 *
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		/*
		 * Instead of a static variable, use JNDI: SessionFactory sessions =
		 * null; try { Context ctx = new InitialContext(); String jndiName =
		 * "java:hibernate/HibernateFactory"; sessions =
		 * (SessionFactory)ctx.lookup(jndiName); } catch (NamingException ex) {
		 * throw new InfrastructureException(ex); } return sessions;
		 */
		return sessionFactory;
	}

	/**
	 * Returns the original Hibernate configuration.
	 *
	 * @return Configuration
	 */
	public static Configuration getConfiguration() {
		return config;
	}

	/**
	 * Rebuild the SessionFactory with the static Configuration.
	 *
	 */
	public static void rebuildSessionFactory() throws HException {
		synchronized (sessionFactory) {
			try {
				sessionFactory = getConfiguration().buildSessionFactory();
			} catch (Exception ex) {
				throw new HException(ex);
			}
		}
	}

	/**
	 * Rebuild the SessionFactory with the given Hibernate Configuration.
	 *
	 * @param cfg
	 */
	public static void rebuildSessionFactory(Configuration cfg)
			throws HException {
		synchronized (sessionFactory) {
			try {
				sessionFactory = cfg.buildSessionFactory();
				config = cfg;
			} catch (Exception ex) {
				throw new HException(ex);
			}
		}
	}

	/**
	 * Retrieves the current Session local to the thread. <p/>If no Session is
	 * open, opens a new Session for the running thread.
	 *
	 * @return Session
	 */
	public static Session getSession() throws HException {
		Session s = (Session) threadSession.get();
		try {
			
			if (s == null) {
				// log.debug("Opening new Session for this thread.");
				if (getInterceptor() != null) {
					// log.debug("Using interceptor: "
					// + getInterceptor().getClass());
					s = getSessionFactory().openSession(getInterceptor());
				} else {
					s = getSessionFactory().openSession();
				}
				
				threadSession.set(s);
				setConnections(1);
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
		return s;
	}

	/**
	 * Closes the Session local to the thread.
	 */
	public static void closeSession() throws HException {
		try {
			commitTransaction();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			rollbackTransaction();
			e.printStackTrace();
		}
		try {
			
			Session s = (Session) threadSession.get();
			threadSession.set(null);
			threadTransaction.set(null);
			if (s != null && s.isOpen()) {
				s.close();
				setConnections(-1);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	private static String getCurrentClassName(){
		Field f;
		try {
			f = Thread.class.getDeclaredField("target");
			f.setAccessible(true);
			if(f.get(Thread.currentThread())==null) return "null";
			return (f.get(Thread.currentThread()).getClass().getName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}
	private static void setConnections(int i)
	{
		synchronized (connections) {
			ConnectionInfo count=connections.get(getCurrentClassName());
			if(count==null){
				count=new ConnectionInfo();
				count.setCurrent(i);
				count.setMax(i);
				
			}
			else{
				count.setCurrent(count.getCurrent()+i);				
			}
			if(count.getCurrent()>count.getMax())
			{
				count.setMax(count.getCurrent());
			}
			connections.put(getCurrentClassName(), count);
		}
	}
	/**
	 * Start a new database transaction.
	 */
	public static void beginTransaction() throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			
			if (tx == null) {
				// log.debug("Starting new database transaction in this
				// thread.");
				//Session =getSession();
				tx = getSession().beginTransaction();
				threadTransaction.set(tx);
				
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}
	public static void setTransactionTimeOut(int timeout){
		Transaction tx = (Transaction) threadTransaction.get();
			
			if (tx !=null) {
				tx.setTimeout(timeout);
				
			}

	}
	/**
	 * Commit the database transaction.
	 */
	public static void commitTransaction() throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				// log.debug("Committing database transaction of this thread.");
				tx.commit();
			}
			threadTransaction.set(null);
		} catch (HibernateException ex) {
			ex.printStackTrace();
			throw new HException(ex);
		}
	}

	/**
	 * Commit the database transaction.
	 */
	public static void rollbackTransaction() throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			threadTransaction.set(null);
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.rollback();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			//closeSession();
		}
	}

	/**
	 * Reconnects a Hibernate Session to the current Thread.
	 *
	 * @param session
	 *            The Hibernate Session to be reconnected.
	 */
	public static void reconnect(Session session) throws HException {
		try {
			session.reconnect();
			threadSession.set(session);
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}

	/**
	 * Disconnect and return Session from current Thread.
	 *
	 * @return Session the disconnected Session
	 */
	public static Session disconnectSession() throws HException {

		Session session = getSession();
		try {
			threadSession.set(null);
			if (session.isConnected() && session.isOpen())
				session.disconnect();
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
		return session;
	}

	/**
	 * Register a Hibernate interceptor with the current thread.
	 * <p>
	 * Every Session opened is opened with this interceptor after registration.
	 * Has no effect if the current Session of the thread is already open,
	 * effective on next close()/getSession().
	 */
	public static void registerInterceptor(Interceptor interceptor) {
		threadInterceptor.set(interceptor);
	}

	private static Interceptor getInterceptor() {
		Interceptor interceptor = (Interceptor) threadInterceptor.get();
		return interceptor;
	}

	/**
	 * <p>
	 * ��������ҵ�����,�����ҵ�һ��Hibernate����
	 *
	 * @param hibernateClass
	 *            Hibernate�Ķ���
	 * @param key
	 *            ����
	 * @return ���ؿգ���ʾ��ݿ��쳣������û���ҵ�����
	 */
	public static Object locate(Class hibernateClass, String key)
			throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {

		}
		try {

			return session.load(hibernateClass, key);

		} catch (HibernateException e) {
			return null;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				//HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * <p>
	 * ���HQL����ѯ���������ͨ��ӿ�ScrollDataInterface���лص����ʺ��û���������Ĳ�ѯ�����
	 * <p>
	 * �������100�����ϵĲ�ѯ���
	 * <p>
	 * ʹ��ʾ��
	 * <p>
	 * ScrollDataInterface back = new ClassA();
	 * <p>
	 * HibernateTool.list(sql,start,max,back);
	 * <p>
	 * ���У�ClassA����ʵ��ScrollDataInterface�ӿ�
	 *
	 * @param sql
	 *            ��ѯ�õ�HQL���
	 * @param start
	 *            ��ѯ���ʼ��ȡ��λ��
	 * @param max
	 *            ���Ĳ�ѯ���
	 * @param data
	 *            ScrollDataInterface��ݽӿڣ������ص�
	 * @throws HibernateException
	 */
	/*public static void list(String sql, int start, int max,
			ScrollDataInterface data) throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {

			Query query = session.createQuery(sql);
			query.setFirstResult(start);
			if (max > 0)
				query.setMaxResults(max);
			ScrollableResults result = query.scroll();
			while (result.next()) {

				Object obj = result.get(0);
				data.doSomething(result.get());
				session.evict(obj);
			}

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}*/

	/**
	 * ��ݲ�ѯ������ʼλ�á��������õ��б?��ѯ��䲻�����
	 *
	 * @param sql
	 *            HQL���
	 * @param start
	 *            ��ʼλ��
	 * @param max
	 *            �������
	 * @return ��ѯ���
	 */
	public static List list(String sql, int start, int max)
			throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {

			Query query = session.createQuery(sql);
			query.setFirstResult(start);
			if (max > 0)
				query.setMaxResults(max);
			return query.list();

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				//HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * <p>
	 * ��ݲ�ѯ������ʼλ�á��������õ��б?��HQL����еĲ�������������¹淶��
	 * <p>
	 * 1��������밴��ǰ��˳���������˳���0��ʼ���簴��HQL˳�����е�һ���Ĳ������Ϊ"arg0",��" field=:arg0"
	 * <p>
	 * 2�������ֵ�ŵ�List�����У�Ҳ���밴˳��һ������ӣ���HQL�в���˳��һ��
	 * <p>
	 * ʾ��
	 * <p>
	 * List values=new Vector();
	 * <p>
	 * String hql="Select c from TDslamNms as c where c.id=:arg0";
	 * <p>
	 * values.add("192.168.1.1");
	 * <p>
	 * List result = HibernateTool.list(hql,values,0,1);
	 *
	 * @param sql
	 *            HQL���
	 * @param start
	 *            ��ʼλ��
	 * @param max
	 *            �������
	 * @param values
	 *            ���ݹ����Ĳ����ղ����˳������
	 * @return
	 */
	public static List list(String sql, List values, int start, int max)
			throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {

			Query query = session.createQuery(sql);
			if (values != null) {
				for (int i = 0; i < values.size(); i++) {
					query.setParameter("arg" + i, values.get(i));
				}
			}
			query.setFirstResult(start);
			if (max > 0)
				query.setMaxResults(max);
			return query.list();

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				//HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * <p>
	 * ��������в����HQL�����в�ѯ����ͨ��ص��ӿڶԽ����в������ʺ����ڴ�������ĵط���
	 * <p>
	 * ��HQL��Values���÷������գ�
	 * <p>
	 * public static List list(String sql,List values,int start,int max)throws
	 * HibernateException
	 * <p>
	 * ��ScrollDataInterface���÷������գ�
	 * <p>
	 * public static void list(String sql,int start,int max,ScrollDataInterface
	 * data)throws HibernateException
	 *
	 * @param sql
	 *            HQL���
	 * @param values
	 *            ��Ӧ����еĲ���
	 * @param start
	 *            ��ѯ����ʼλ��
	 * @param max
	 *            ��ѯ�Ľ���������
	 * @param data
	 *            �ص��ӿ�
	 * @throws HibernateException
	 *
	public static void list(String sql, List values, int start, int max,
			ScrollDataInterface data) throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {

			Query query = session.createQuery(sql);
			if (values != null) {
				for (int i = 0; i < values.size(); i++) {
					query.setParameter("arg" + i, values.get(i));
				}
			}
			query.setFirstResult(start);
			if (max > 0)
				query.setMaxResults(max);
			ScrollableResults result = query.scroll();
			while (result.next()) {

				Object obj = result.get(0);
				data.doSomething(result.get());
				session.evict(obj);
			}

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}
*/
	/**
	 * �޸���ݣ������ύ���񣬵����뽫�Ự��Ϊ��������
	 *
	 * @param obj
	 *            ����
	 * @param session
	 *            �Ự
	 * @throws HibernateException
	 */
	public static void update(Object obj, Session session)
			throws HibernateException {
		session.update(obj);
		session.clear();
	}

	/**
	 * �޸���ݣ�����Ϊ�����ύ
	 *
	 * @param obj
	 *            ����
	 * @throws HibernateException
	 */
	public static void update(Object obj) throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				//HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * ��������õ�Count�Ĳ�ѯ���
	 *
	 * @param sql
	 *            ��������" TDslamNms as c where c.dslamMan='02'"
	 * @return ��ѯ������
	 * @throws HibernateException
	 */
	/*public static int count(String sql) throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {
			return ((Integer) session.iterate("Select count(*) from " + sql)
					.next()).intValue();

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}
*/
	/**
	 * <p>
	 * ��������õ�Count�Ĳ�ѯ���Count��ѯ�������в���Ĵ���
	 * <p>
	 * ���ڲ���Ĵ��ݣ�ͬ
	 * <p>
	 * public static List list(String sql,List values,int start,int max)throws
	 * HibernateException
	 *
	 * @param sql
	 *            ��������" TDslamNms as c where c.dslamMan=:arg0"
	 * @return ��ѯ������
	 * @throws HibernateException
	 */
	public static int count(String sql, List values) throws HibernateException {
		Session session = getSession(); // �õ�Hibernate Session
		if (session == null) {
			throw new HibernateException("������ݿ�ʧ��");
		}
		try {
			Query query = session.createQuery("Select count(*) from " + sql);
			if (values != null) {
				for (int i = 0; i < values.size(); i++) {
					query.setParameter("arg" + i, values.get(i));
				}
			}
			List result = query.list();
			return ((Integer) result.get(0)).intValue();
			// return ((Integer)session.iterate("Select count(*)
			// "+sql).next()).intValue();

		} catch (HibernateException e) {
			throw e;
		} finally {
			try {
				// session.close(); // �ر�Hibernate �Ự
				//HibernateTool.closeSession();
			} catch (Exception e) {

			}
		}
	}

	public static void main(String[] args) {
		
	}
}
