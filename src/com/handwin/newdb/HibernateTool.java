/*
 * �������� 2005-8-18
 *
 * TODO Ҫ��Ĵ���ɵ��ļ���ģ�壬��ת��
 * ���� �� ��ѡ�� �� Java �� ������ʽ �� ����ģ��
 */
package com.handwin.newdb;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.function.ClassicAvgFunction;
import org.hibernate.dialect.function.ClassicCountFunction;
import org.hibernate.dialect.function.ClassicSumFunction;



/**
 * Basic Hibernate helper class, handles SessionFactory, Session and
 * Transaction.
 * <p>
 * Uses a static initializer for the initial SessionFactory creation and holds
 * Session and Transactions in thread local variables. All exceptions are
 * wrapped in an unchecked InfrastructureException.
 * 
 * @author christian@hibernate.org
 */
public class HibernateTool {

	private static Logger log = Logger.getLogger(HibernateTool.class);

	private static Map<String,Configuration> config=new HashMap<String,Configuration>();

	private static Map<String,SessionFactory> sessionFactory=new HashMap<String,SessionFactory>();

	private static final Map<String,ThreadLocal> threadSession = new HashMap<String,ThreadLocal>();

	private static final Map<String,ThreadLocal> threadTransaction = new HashMap<String,ThreadLocal>();

	private static final Map<String,ThreadLocal> threadInterceptor = new HashMap<String,ThreadLocal>();

	// Create the initial SessionFactory from the default configuration files
//	static {
//		try {
//			//config = new Configuration();
//
//			// Properties prop = new Properties();
//			// prop.setProperty("hibernate.connection.username",Configure.dbUser);
//			// prop.setProperty("hibernate.connection.password",Configure.dbPassword);
//			// prop.setProperty("hibernate.connection.url",Configure.dbURL);
//			// config.addProperties(prop);
//
//			sessionFactory = config.configure().buildSessionFactory();
//			// We could also let Hibernate bind it to JNDI:
//			// configuration.configure().buildSessionFactory()
//		} catch (Throwable ex) {
//			// We have to catch Throwable, otherwise we will miss
//			// NoClassDefFoundError and other subclasses of Error
//			log.error("Building SessionFactory failed.", ex);
//			ex.printStackTrace();
//			throw new ExceptionInInitializerError(ex);
//		}
//	}
	public static void initConfig(String name,URL url)
	{
		Configuration conf = new Configuration().configure(url);
		conf.addSqlFunction( "count", new ClassicCountFunction());
		conf.addSqlFunction( "avg", new ClassicAvgFunction());
		conf.addSqlFunction( "sum", new ClassicSumFunction());
		SessionFactory factory=conf.buildSessionFactory();
		sessionFactory.put(name, factory);
		config.put(name, conf);
		threadTransaction.put(name, new ThreadLocal());
		threadSession.put(name, new ThreadLocal());
	}

	/**
	 * Returns the SessionFactory used for this static class.
	 *
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionFactory(String name) {
		/*
		 * Instead of a static variable, use JNDI: SessionFactory sessions =
		 * null; try { Context ctx = new InitialContext(); String jndiName =
		 * "java:hibernate/HibernateFactory"; sessions =
		 * (SessionFactory)ctx.lookup(jndiName); } catch (NamingException ex) {
		 * throw new InfrastructureException(ex); } return sessions;
		 */
		return sessionFactory.get(name);
	}

	/**
	 * Returns the original Hibernate configuration.
	 *
	 * @return Configuration
	 */
	public static Configuration getConfiguration(String name) {
		return config.get(name);
	}

	/**
	 * Rebuild the SessionFactory with the static Configuration.
	 *
	 */
	public static void rebuildSessionFactory(String name) throws HException {
		synchronized (sessionFactory) {
			try {
				SessionFactory factory = getConfiguration(name).buildSessionFactory();
				sessionFactory.put(name, factory);
			} catch (Exception ex) {
				throw new HException(ex);
			}
		}
	}


	/**
	 * Retrieves the current Session local to the thread. <p/>If no Session is
	 * open, opens a new Session for the running thread.
	 *
	 * @return Session
	 */
	public static Session getSession(String name) throws HException {
		Session s = (Session) threadSession.get(name).get();
		try {
			
			if (s == null) {
				// log.debug("Opening new Session for this thread.");				
				s = getSessionFactory(name).openSession();				
				threadSession.get(name).set(s);
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
		return s;
	}

	/**
	 * Closes the Session local to the thread.
	 */
	public static void closeSession(String name) throws HException {
		commitTransaction(name);
		try {
			Session s = (Session) threadSession.get(name).get();
			threadSession.get(name).set(null);
			threadTransaction.get(name).set(null);
			if (s != null && s.isOpen()) {
				s.close();
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}

	/**
	 * Start a new database transaction.
	 */
	public static void beginTransaction(String name) throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get(name).get();
		try {
			if (tx == null) {
				// log.debug("Starting new database transaction in this
				// thread.");
				//Session =getSession();
				tx = getSession(name).beginTransaction();
				threadTransaction.get(name).set(tx);
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}

	/**
	 * Commit the database transaction.
	 */
	public static void commitTransaction(String name) throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get(name).get();
		try {
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				// log.debug("Committing database transaction of this thread.");
				tx.commit();
			}
			threadTransaction.get(name).set(null);
		} catch (HibernateException ex) {
			ex.printStackTrace();
			rollbackTransaction(name);
			throw new HException(ex);
		}
	}

	/**
	 * Commit the database transaction.
	 */
	public static void rollbackTransaction(String name) throws HException {
		
		Transaction tx = (Transaction) threadTransaction.get(name).get();
		try {
			threadTransaction.get(name).set(null);
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.rollback();
			}
		} catch (HibernateException ex) {
			throw new HException(ex);
		} finally {
			//closeSession();
		}
	}

	public static void setTransactionTimeOut(String name,int timeout){
		Transaction tx = (Transaction) threadTransaction.get(name).get();
			
			if (tx !=null) {
				tx.setTimeout(timeout);
				
			}

	}
	

	public static void main(String[] args) {
		
	}
}
