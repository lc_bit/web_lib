package com.handwin.newdb;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class Dao {

	private static final Map<String,Boolean> daoMap=new HashMap<String,Boolean>();
	private String name;
	public Dao(String name)
	{
		if(daoMap.get(name)==null)
		{
			initConfig(name);
			
		}
		this.name=name;
		HibernateTool.beginTransaction(name);
	}
	public void insertOrUpdate(INativeSqlEntity entity){
		String tableName=entity.getClass().getSimpleName();
		Query query = HibernateTool.getSession(name).getNamedQuery(tableName+"_insert");
		Object[] values=entity.toArrayValues();
		for (int i = 0; i < values.length; i++) {
			query.setParameter("arg"+i, values[i]);
		}
		query.executeUpdate();
	}
	public void insert(Object dpo) throws HException{
		try {
			HibernateTool.getSession(name).save(dpo);
		} catch (HibernateException ex) {
			ex.printStackTrace();
			throw new HException(ex);
		}
	}
	public void save(Object dpo) throws HException{

		try {
			HibernateTool.getSession(name).saveOrUpdate(dpo);
		} catch (HibernateException ex) {
			throw new HException(ex);
		}

	}
	/**
	 * @param timeout
	 * 单位：秒
	 */
	public void setTransactionTimeOut(int timeout){
		HibernateTool.setTransactionTimeOut(name, timeout);
	}
	public void commit() {
		// logger.info("DataService.commit begin");
		HibernateTool.commitTransaction(name);

		// logger.info("DataService.commit end");
	}
	public void delete(Object po) throws HException{
		try {
			HibernateTool.getSession(name).delete(po);
		} catch (HibernateException ex) {
			throw new HException(ex);
		}
	}
public void rollBack() {
		// logger.info("DataService.rollBack begin");
		HibernateTool.rollbackTransaction(name);
		// logger.info("DataService.rollBack end");
	}

	public void close() {
		// logger.info("DataService.close begin");
		
		HibernateTool.closeSession(name);
		// logger.info("DataService.close end");
	}
public Object locate(Class<?> clz,Object value[],String column[])
	{
		Session session = HibernateTool.getSession(name);
		try
		{
			Criteria query=session.createCriteria(clz);
			
			for(int i=0;i<value.length;i++)
				query.add(Restrictions.eq(column[i], value[i]));
			query.setMaxResults(1);
			List list=query.list();
			if(list.size()>0)
				return list.get(0);
		}catch(HibernateException e)
		{
			//e.printStackTrace();
			return null;
		}
		return null;
	}
	public Object locateLock(Class<?> clz,Object value[],String column[])
	{
		Session session = HibernateTool.getSession(name);
		try
		{
			Criteria query=session.createCriteria(clz);
			
			for(int i=0;i<value.length;i++)
				query.add(Restrictions.eq(column[i], value[i]));
			query.setMaxResults(1);
			List list=query.list();
			if(list.size()>0)
			{
				Object rlt=list.get(0);
				session.lock(rlt, LockMode.UPGRADE);
				return rlt;
			}
		}catch(HibernateException e)
		{
			return null;
		}
		return null;
	}
	public void flush()
	{
		Session session = HibernateTool.getSession(name);
		try
		{
			session.flush();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void clear()
	{
		Session session = HibernateTool.getSession(name);
		try
		{
			session.clear();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 根据物理ID，获取Hibernate对象
	 * @param clz Hibernate类
	 * @param id 主键
	 * @return 对象，没有找到返回Null
	 */
	public Object locate(Class<?> clz, Serializable id) {
		if(id==null)
			return null;
		Session session = HibernateTool.getSession(name);
		Object bpo = null;

		try {
			bpo = session.get(clz, id);
			//bpo.toString();
		} catch (HibernateException e) {
			//e.printStackTrace();
			return null;
			// throw new HException(e);
		}
		return bpo;
	}
	public Object locateLock(Class<?> clz, Serializable id) {
		if(id==null)
			return null;
		Session session = HibernateTool.getSession(name);
		Object bpo = null;

		try {
			bpo = session.get(clz, id, LockMode.UPGRADE);
			//bpo.toString();
		} catch (HibernateException e) {
			//e.printStackTrace();
			return null;
			// throw new HException(e);
		}
		return bpo;
	}

	public Connection getConnection() {
		try {
			return HibernateTool.getSession(name).connection();
		} catch (HibernateException e) {
			// TODO �Զ���� catch ��
			e.printStackTrace();
			throw new HException(e.getMessage());
		}
	}

	public Session getSession() {
		return HibernateTool.getSession(name);
	}	
	private void initConfig(String name)
	{
		
		HibernateTool.initConfig(name, this.getClass().getClassLoader().getResource("hibernate_"+name+".cfg.xml"));
		daoMap.put(name, true);
	}
	public static void main(String[] args) {
		
		Field[] fields=Thread.class.getDeclaredFields();
		for(Field f:fields)
		{
			//System.out.println();
		}
		for(int i=0;i<5;i++)
		{
		
			new Thread(new Runnable(){
				public void run()
				{
					Field f;
					try {
						f = Thread.class.getDeclaredField("target");
						f.setAccessible(true);
						System.out.println(f.get(Thread.currentThread()).getClass().getName());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
					//System.out.println(Thread.currentThread().set);
//					for(int i=0;i<1000;i++)
//					{
//						
//					Dao dao1=new Dao("test1");
//					System.out.println(i);
//					try {
//						Thread.sleep(100);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					dao1.close();
//					}
					
					
				}
			}).start();
		}
		
	}
}
